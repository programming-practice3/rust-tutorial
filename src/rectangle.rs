
pub struct Rectangle {
    pub name: String,
    pub length: i32,
    pub width: i32,
}

impl Rectangle {
    pub fn area(&self) -> i32 {
        self.length * self.width
    }
    pub fn perimeter(&self) -> i32 {
        2 * self.length + 2 * self.width
    }
}

