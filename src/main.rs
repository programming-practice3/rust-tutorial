mod rectangle;
use std::io;
use rectangle::Rectangle;

fn main() {
    /* Using structs */

    let rectangle = Rectangle { name: String::from("rectangle 1"), length: 8, width: 10};
    println!("\nRectangle:");
    println!("name: {}", rectangle.name);
    println!("length: {}", rectangle.length);
    println!("width: {}", rectangle.width);
    println!("area: {}", rectangle.area());
    println!("perimeter {}", rectangle.perimeter());

    /* Using loops */

    println!("\nCounting:");
    println!("\nWhile loop ->");
    let mut n = 1;

    while n <= 10 {
        println!("{}", n);
        n += 1;
    }
    println!("\nFor loop with range ->");
    for i in 1..11 {
        println!("{}", i);
    }


    /* Error checking
        1. get user input
        2. trimming and parsing data
        Note: this part gets a little complicated
    */
    
    println!("\nGuess the number!");
    println!("Please input your guess.");
    let mut guess = String::new();

    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");

    let trimmed = guess.trim();
    let result = guess.trim().parse::<u32>();
    match result {
        Ok(i) => println!("You guessed {}", i),
        Err(..) => println!("not integer: {}", trimmed),
    };
}
