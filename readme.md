# rust-tutorial 
My code I created to pratice learning the Rust Programming Language.

## Using Cargo
Create a new cargo package
```
cargo new project_name
```

Initialize the cargo package in an existing directory
```
cargo init 
```

Run a binary of the local package
```
cargo run
```

## Resources for Learning Rust Programming Language
* [Learn Rust](https://www.rust-lang.org/learn)
* [Rustdoc Book](https://doc.rust-lang.org/book/)
* [Paperbook or Ebook of The Rust Programming Language](https://nostarch.com/Rust2018)

